#ifndef FE_COM_PAR_H
#define FE_COM_PAR_H

#include <QApplication>
#include <QWidget>
#include <QFrame>
#include <QLabel>
#include <QFont>
#include <QPushButton>
#include <QMenu>
#include <QAction>
#include <QDialog>
#include <QSpinBox>
#include <QLineEdit>
#include <QMessageBox>

// FIXME: Only one USB-device can be used.
//        We need a selection box for all connected USB-devices!

class FeComPar : public QWidget
{
	Q_OBJECT

public:
	static const int FeCom = 0;
	static const int FeUsb = 1;
	static const int FeTcp = 2;

	FeComPar(QWidget *parent = NULL);
	~FeComPar();

	int        communicationType       (void) const   { return m_comType; }
	void       setCommunicationType    (int type);
	int        connectionHandle        (void) const   { return m_connectionHandle; }
	QString    connectionDescriptor    (void) const   { return *m_connectionDescr; }
	void       showOpenCloseButton     (bool visible) { btnOpenClose->setVisible(visible); }

	QString    comPort                 (void) const { return btnSelComPort->text(); }
	bool       setComPort              (const QString & port);
	QString    baudrate                (void) const { return btnSelBaudrate->text(); }
	bool       setBaudrate             (const QString & baudrate);
	QString    frame                   (void) const { return btnSelFrame->text(); }
	bool       setFrame                (const QString & frame);
	int        blockTimeout            (void) const { return sbxBlkTimeout->value(); }
	void       setBlockTimeout         (int val)    { sbxBlkTimeout->setValue(val); }
	int        charTimeoutMultiplier   (void) const { return sbxCharTimeoutMul->value(); }
	void       setCharTimeoutMultiplier(int val)    { sbxCharTimeoutMul->setValue(val); }
	int        txTimeCtrl              (void) const { return sbxTxTimeCtrl->value(); }
	void       setTxTimeCtrl           (int val)    { sbxTxTimeCtrl->setValue(val); }

	int        usbTimeout              (void) const { return sbxUsbTimeout->value(); }
	void       setUsbTimeout           (int val)    { sbxUsbTimeout->setValue(val); }

	QString    ipAddr                  (void) const { return editIpAddr->text(); }
	void       setIpAddr               (const QString & ipAddr) { editIpAddr->setText(ipAddr); }
	int        tcpPort                 (void) const { return sbxTcpPort->value(); }
	void       setTcpPort              (int port)   { sbxTcpPort->setValue(port); }
	int        tcpTimeout              (void) const { return sbxTcpTimeout->value(); }
	void       setTcpTimeout           (int val)    { sbxTcpTimeout->setValue(val); }

	static bool isConnection           (int comHandle) { return comHandle > 0; }
	static bool isComConnection        (int comHandle) { return (comHandle & 0xF0000000) == 0; }
	static bool isUsbConnection        (int comHandle) { return (comHandle & 0x10000000) != 0; }
	static bool isTcpConnection        (int comHandle) { return (comHandle & 0x20000000) != 0; }

public slots:
	void       setCom(void);
	void       setUsb(void);
	void       setTcp(void);
	int        openConnection(void); // returns a connection handle
	void       closeConnection(void);

signals:
	void       connectionStateChanged(void);

private:
	int                    m_comType;
	int                    m_connectionHandle;
	QString               *m_connectionDescr;
	QPushButton           *btnSelCom;
	QMenu                 *menSelCom;
	QAction               *actUseCom;
	QAction               *actUseUsb;
	QAction               *actUseTcp;

	QPushButton           *btnOpenClose;

/* serial */
	QFrame                *frmCom;
	QLabel                *lblCom;

	QLabel                *lblSelComPort;
	QPushButton           *btnSelComPort;
	QMenu                 *menSelComPort;
	QVector<QAction*>     *vecComPorts;

	QLabel                *lblSelBaudrate;
	QPushButton           *btnSelBaudrate;
	QMenu                 *menSelBaudrate;
	QVector<QAction*>     *vecBaudrates;

	QLabel                *lblSelFrame;
	QPushButton           *btnSelFrame;
	QMenu                 *menSelFrame;
	QVector<QAction*>     *vecFrames;

	QLabel                *lblBlkTimeout;
	QSpinBox              *sbxBlkTimeout;

	QLabel                *lblCharTimeoutMul;
	QSpinBox              *sbxCharTimeoutMul;

	QLabel                *lblTxTimeCtrl;
	QSpinBox              *sbxTxTimeCtrl;

/* USB */
	QFrame                *frmUsb;
	QLabel                *lblUsb;

	QLabel                *lblUsbTimeout;
	QSpinBox              *sbxUsbTimeout;

/* TCP */
	QFrame                *frmTcp;
	QLabel                *lblTcp;

	QLabel                *lblIpAddr;
	QLineEdit             *editIpAddr;

	QLabel                *lblTcpPort;
	QSpinBox              *sbxTcpPort;

	QLabel                *lblTcpTimeout;
	QSpinBox              *sbxTcpTimeout;

	void            arrangeMyWidgets(void);

private slots:
	void            selectComPort(QAction *act);
	void            selectBaudrate(QAction *act);
	void            selectFrame(QAction *act);
	void            dialogOpenClose(void);
};

class FeComParDialog : public QDialog
{
	Q_OBJECT

public:
	FeComParDialog(QWidget *parent = NULL);
	~FeComParDialog();

	int        communicationType       (void) const   { return comPar->communicationType(); }
	void       setCommunicationType    (int type)     { comPar->setCommunicationType(type); }
	int        connectionHandle        (void) const   { return comPar->connectionHandle(); }
	QString    connectionDescriptor    (void) const   { return comPar->connectionDescriptor(); }
	void       showOpenCloseButton     (bool visible) { comPar->showOpenCloseButton(visible); }
	int        exec                    (void);

	QString    comPort                 (void) const { return comPar->comPort(); }
	bool       setComPort              (const QString & port) { return comPar->setComPort(port); }
	QString    baudrate                (void) const { return comPar->baudrate(); }
	bool       setBaudrate             (const QString & baudrate) { return comPar->setBaudrate(baudrate); }
	QString    frame                   (void) const { return comPar->frame(); }
	bool       setFrame                (const QString & frame) { return comPar->setFrame(frame); }
	int        blockTimeout            (void) const { return comPar->blockTimeout(); }
	void       setBlockTimeout         (int val)    { comPar->setBlockTimeout(val); }
	int        charTimeoutMultiplier   (void) const { return comPar->charTimeoutMultiplier(); }
	void       setCharTimeoutMultiplier(int val)    { comPar->setCharTimeoutMultiplier(val); }
	int        txTimeCtrl              (void) const { return comPar->txTimeCtrl(); }
	void       setTxTimeCtrl           (int val)    { comPar->setTxTimeCtrl(val); }

	int        usbTimeout              (void) const { return comPar->usbTimeout(); }
	void       setUsbTimeout           (int val)    { comPar->setUsbTimeout(val); }

	QString    ipAddr                  (void) const { return comPar->ipAddr(); }
	void       setIpAddr               (const QString & ipAddr) { comPar->setIpAddr(ipAddr); }
	int        tcpPort                 (void) const { return comPar->tcpPort(); }
	void       setTcpPort              (int port)   { comPar->setTcpPort(port); }
	int        tcpTimeout              (void) const { return comPar->tcpTimeout(); }
	void       setTcpTimeout           (int val)    { comPar->setTcpTimeout(val); }

public slots:
	int        openConnection(void)  { return comPar->openConnection(); }
	void       closeConnection(void) { comPar->closeConnection(); }

signals:
	void       connectionStateChanged(void);

private:
	FeComPar              *comPar;
	QPushButton           *btnOk;
	QPushButton           *btnCancel;

	void            arrangeMyWidgets(void);
};

#endif // FE_COM_PAR_H
