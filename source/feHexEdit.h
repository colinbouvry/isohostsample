#ifndef FE_HEX_EDIT_H
#define FE_HEX_EDIT_H

#include <QScrollArea>
#include <QLabel>
#include <QLineEdit>
#include <QVector>
#include <QByteArray>



class FeHexBlock : public QWidget
{
	Q_OBJECT

public:
	FeHexBlock(unsigned int blockIndex, unsigned int bytesPerBlock, QWidget *parent = NULL);
	~FeHexBlock();

	void         setBytePerBlock(unsigned int bytesPerBlock);
	unsigned int bytesPerBlock  (void) { return m_bytesPerBlock; }
	void         setBinary      (const QByteArray & bin);
	QByteArray   binary         (void);

private:
	unsigned int  m_bytesPerBlock;
	QString       m_hexInputMask;
	QString       m_ascInputMask;
	QLabel       *lblBlock;
	QLineEdit    *ledHex;
	QLineEdit    *ledAsc;

	void       arrangeMyWidgets(void);
	QByteArray hexTextToBin(const QString & hexText);
	QByteArray ascTextToBin(const QString & ascText);
	QString    binToHexText(const QByteArray & bin);
	QString    binToAscText(const QByteArray & bin);

private slots:
	void hexTextEdited(const QString & text);
	void ascTextEdited(const QString & text);
};



class FeHexEdit : public QScrollArea 
{
	Q_OBJECT

public:
	FeHexEdit(unsigned int maxBlocks, unsigned int bytesPerBlock, QWidget *parent = NULL);
	~FeHexEdit();

	void         configure    (unsigned int maxBlocks, unsigned int bytesPerBlock);
	unsigned int maxBlocks    (void) { return m_blocks.size(); }
	unsigned int bytesPerBlock(void) { return m_bytesPerBlock; }

	void resetBinaryData(void);
	void setBinaryData(const QVector<unsigned char> & data, unsigned int firstBlock, unsigned int bytesPerBlock);
	void getBinaryData(QVector<unsigned char> & data, unsigned int firstBlock, unsigned int numBlocks);

private:
	unsigned int           m_bytesPerBlock;
	QWidget               *m_edit;
	QVector<FeHexBlock*>   m_blocks;

	void    clear();
	void    setBlocks(unsigned int maxBlocks, unsigned int bytesPerBlock);
	void    arrangeMyWidgets(void);
};

#endif // FE_HEX_EDIT_H
