#include "feComPar.h"

#ifdef _MSC_VER
#include <windows.h>
#endif

#include "fecom.h"
#include "feusb.h"
#include "fetcp.h"

#define FECOMPAR_WIDTH  400
#define FECOMPAR_HEIGHT 300


FeComPar::FeComPar(QWidget *parent)
	: QWidget(parent)
{
	QAction *act;
	int n;

	btnSelCom = new QPushButton("Communication type", this);
	QFont boldFont = btnSelCom->font();
	boldFont.setBold(true);
	btnSelCom->setFont(boldFont);

	menSelCom = new QMenu(this);
	actUseCom = menSelCom->addAction("COM");
	actUseUsb = menSelCom->addAction("USB");
	actUseTcp = menSelCom->addAction("TCP");
	connect(actUseCom, SIGNAL(triggered()), this, SLOT(setCom()));
	connect(actUseUsb, SIGNAL(triggered()), this, SLOT(setUsb()));
	connect(actUseTcp, SIGNAL(triggered()), this, SLOT(setTcp()));
	btnSelCom->setMenu(menSelCom);

	btnOpenClose = new QPushButton("Open connection", this);
	btnOpenClose->hide();
	connect(btnOpenClose, SIGNAL(clicked()), this, SLOT(dialogOpenClose()));

	frmCom = new QFrame(this);
	frmCom->setFrameStyle(QFrame::Panel | QFrame::Raised);
	frmCom->setLineWidth(2);

	lblCom = new QLabel("COM", frmCom);
	lblCom->setAlignment(Qt::AlignTop | Qt::AlignLeft);
	lblCom->setFont(boldFont);

	vecComPorts = new QVector<QAction*>(0);
	lblSelComPort = new QLabel("Com-Port", frmCom);
	lblSelComPort->setAlignment(Qt::AlignVCenter | Qt::AlignRight);
	btnSelComPort = new QPushButton("", frmCom);
	menSelComPort = new QMenu(this);
	for (n = 1; n <= 256; n++) {
		if (FECOM_DetectPort(n) == 0) {
			act = menSelComPort->addAction(QString("COM%1").arg(n));
			vecComPorts->append(act);
		}
	}
	btnSelComPort->setMenu(menSelComPort);
	connect(menSelComPort, SIGNAL(triggered(QAction*)), this, SLOT(selectComPort(QAction*)));
	if (vecComPorts->size() > 0)
		selectComPort(vecComPorts->at(0));

    vecBaudrates = new QVector<QAction*>(0);
	lblSelBaudrate = new QLabel("Baudrate", frmCom);
	lblSelBaudrate->setAlignment(Qt::AlignVCenter | Qt::AlignRight);
	btnSelBaudrate = new QPushButton("", frmCom);
	menSelBaudrate = new QMenu(this);
	act = menSelBaudrate->addAction("9600");   vecBaudrates->append(act);
	act = menSelBaudrate->addAction("19200");  vecBaudrates->append(act);
	act = menSelBaudrate->addAction("38400");  vecBaudrates->append(act);
	act = menSelBaudrate->addAction("57600");  vecBaudrates->append(act);
	act = menSelBaudrate->addAction("115200"); vecBaudrates->append(act);
	act = menSelBaudrate->addAction("230400"); vecBaudrates->append(act);
	btnSelBaudrate->setMenu(menSelBaudrate);
	connect(menSelBaudrate, SIGNAL(triggered(QAction*)), this, SLOT(selectBaudrate(QAction*)));
	selectBaudrate(vecBaudrates->at(2));

	vecFrames = new QVector<QAction*>(0);
	lblSelFrame = new QLabel("Frame", frmCom);
	lblSelFrame->setAlignment(Qt::AlignVCenter | Qt::AlignRight);
	btnSelFrame = new QPushButton("", frmCom);
	menSelFrame = new QMenu(this);
	act = menSelFrame->addAction("8N1"); vecFrames->append(act);
	act = menSelFrame->addAction("8E1"); vecFrames->append(act);
	act = menSelFrame->addAction("8O1"); vecFrames->append(act);
	btnSelFrame->setMenu(menSelFrame);
	connect(menSelFrame, SIGNAL(triggered(QAction*)), this, SLOT(selectFrame(QAction*)));
	selectFrame(vecFrames->at(1));

	lblBlkTimeout = new QLabel("Block timeout", frmCom);
	lblBlkTimeout->setAlignment(Qt::AlignVCenter | Qt::AlignRight);
	sbxBlkTimeout = new QSpinBox(frmCom);
	sbxBlkTimeout->setRange(10, INT_MAX);
	sbxBlkTimeout->setValue(5000);
	sbxBlkTimeout->setSuffix(" ms");

	lblCharTimeoutMul = new QLabel("Char timeout multiplier", frmCom);
	lblCharTimeoutMul->setAlignment(Qt::AlignVCenter | Qt::AlignRight);
	sbxCharTimeoutMul = new QSpinBox(frmCom);
	sbxCharTimeoutMul->setRange(1, INT_MAX);
	sbxCharTimeoutMul->setValue(1);

	lblTxTimeCtrl = new QLabel("Tx time control", frmCom);
	lblTxTimeCtrl->setAlignment(Qt::AlignVCenter | Qt::AlignRight);
	sbxTxTimeCtrl = new QSpinBox(frmCom);
	sbxTxTimeCtrl->setRange(1, INT_MAX);
	sbxTxTimeCtrl->setValue(10);
	sbxTxTimeCtrl->setSuffix(" ms");

	frmUsb = new QFrame(this);
	frmUsb->setFrameStyle(QFrame::Panel | QFrame::Raised);
	frmUsb->setLineWidth(2);

	lblUsb = new QLabel("USB", frmUsb);
	lblUsb->setAlignment(Qt::AlignTop | Qt::AlignLeft);
	lblUsb->setFont(boldFont);

	lblUsbTimeout = new QLabel("Timeout", frmUsb);
	lblUsbTimeout->setAlignment(Qt::AlignVCenter | Qt::AlignRight);
	sbxUsbTimeout = new QSpinBox(frmUsb);
	sbxUsbTimeout->setRange(10, INT_MAX);
	sbxUsbTimeout->setValue(5000);
	sbxUsbTimeout->setSuffix(" ms");

	frmTcp = new QFrame(this);
	frmTcp->setFrameStyle(QFrame::Panel | QFrame::Raised);
	frmTcp->setLineWidth(2);

	lblTcp = new QLabel("TCP", frmTcp);
	lblTcp->setAlignment(Qt::AlignTop | Qt::AlignLeft);
	lblTcp->setFont(boldFont);

	lblIpAddr = new QLabel("IP-Address / Hostname", frmTcp);
	lblIpAddr->setAlignment(Qt::AlignVCenter | Qt::AlignRight);
	editIpAddr = new QLineEdit("192.168.3.", frmTcp);

	lblTcpPort = new QLabel("Port", frmTcp);
	lblTcpPort->setAlignment(Qt::AlignVCenter | Qt::AlignRight);
	sbxTcpPort = new QSpinBox(frmTcp);
	sbxTcpPort->setRange(1, 0xFFFF);
	sbxTcpPort->setValue(10001);

	lblTcpTimeout = new QLabel("Timeout", frmTcp);
	lblTcpTimeout->setAlignment(Qt::AlignVCenter | Qt::AlignRight);
	sbxTcpTimeout = new QSpinBox(frmTcp);
	sbxTcpTimeout->setRange(10, INT_MAX);
	sbxTcpTimeout->setValue(5000);
	sbxTcpTimeout->setSuffix(" ms");

	arrangeMyWidgets();
	m_connectionHandle = -1;
	m_connectionDescr = new QString("closed");
	setCom();
}

FeComPar::~FeComPar()
{
	delete vecComPorts;
	delete vecBaudrates;
	delete vecFrames;
	delete m_connectionDescr;
}

void FeComPar::setCommunicationType(int type)
{
	switch (type) {
		case FeComPar::FeCom:
			setCom();
			break;
		case FeComPar::FeUsb:
			setUsb();
			break;
		case FeComPar::FeTcp:
			setTcp();
			break;
		default:
			break;
	}
}

void FeComPar::setCom(void)
{
	m_comType = FeCom;
	frmCom->show();
	frmUsb->hide();
	frmTcp->hide();
}

void FeComPar::setUsb(void)
{
	m_comType = FeUsb;
	frmCom->hide();
	frmUsb->show();
	frmTcp->hide();
}

void FeComPar::setTcp(void)
{
	m_comType = FeTcp;
	frmCom->hide();
	frmUsb->hide();
	frmTcp->show();
}

int FeComPar::openConnection(void)
{
	char str[256];
	QByteArray qba;
	char *cp;
	int n;

	switch (m_comType) {
		case FeComPar::FeCom:
			qba = comPort().toLatin1();
			cp = qba.data() + 3; // works only under windows
			m_connectionHandle = FECOM_OpenPort(cp);
			if (m_connectionHandle < 0) {
				*m_connectionDescr = "failed";
				break;
			}

			*m_connectionDescr = comPort();
			qba = baudrate().toLatin1();
			cp = qba.data();
			FECOM_SetPortPara(m_connectionHandle, (char *)"BAUD", cp);
			qba = frame().toLatin1();
			cp = qba.data();
			FECOM_SetPortPara(m_connectionHandle, (char *)"FRAME", cp);
			sprintf(str, "%i", blockTimeout());
			FECOM_SetPortPara(m_connectionHandle, (char *)"TIMEOUT", str);
			sprintf(str, "%i", charTimeoutMultiplier());
			FECOM_SetPortPara(m_connectionHandle, (char *)"CHARTIMEOUTMPY", str);
			sprintf(str, "%i", txTimeCtrl());
			FECOM_SetPortPara(m_connectionHandle, (char *)"TXTIMECONTROL", str);
			break;

		case FeComPar::FeUsb:
			m_connectionHandle = FEUSB_ScanAndOpen(FEUSB_SCAN_FIRST, NULL);
			if (m_connectionHandle < 0) {
				*m_connectionDescr = "failed";
				break;
			}

			*m_connectionDescr = "USB";
			sprintf(str, "%i", usbTimeout());
			FEUSB_SetDevicePara(m_connectionHandle, (char *)"TIMEOUT", str);
			break;

		case FeComPar::FeTcp:
			qba = ipAddr().toLatin1();
			cp = qba.data();
			n = tcpPort();
			m_connectionHandle = FETCP_Connect(cp, n);
			if (m_connectionHandle < 0) {
				*m_connectionDescr = "failed";
				break;
			}

			*m_connectionDescr = QString("%1:%2").arg(ipAddr()).arg(tcpPort());
			sprintf(str, "%i", tcpTimeout());
			FETCP_SetSocketPara(m_connectionHandle, (char *)"TIMEOUT", str);
			break;

		default:
			m_connectionHandle = -1;
			*m_connectionDescr = "closed";
			break;
	}
	emit connectionStateChanged();
	return m_connectionHandle;
}

void FeComPar::closeConnection(void)
{
	if (m_connectionHandle < 0)
		return;

	if (isComConnection(m_connectionHandle)) {
		FECOM_ClosePort(m_connectionHandle);
	}
	else if (isUsbConnection(m_connectionHandle)) {
		FEUSB_CloseDevice(m_connectionHandle);
	}
	else if (isTcpConnection(m_connectionHandle)) {
		FETCP_DisConnect(m_connectionHandle);
	}
	m_connectionHandle = -1;
	*m_connectionDescr = "closed";
	emit connectionStateChanged();
}

bool FeComPar::setComPort(const QString & port)
{
	int num, n;
	for (num = vecComPorts->size(), n = 0; n < num; n++) {
		if (vecComPorts->at(n)->text() == port) {
			selectComPort(vecComPorts->at(n));
			return true;
		}
	}
	return false;
}

bool FeComPar::setBaudrate(const QString & baudrate)
{
	int num, n;
	for (num = vecBaudrates->size(), n = 0; n < num; n++) {
		if (vecBaudrates->at(n)->text() == baudrate) {
			selectBaudrate(vecBaudrates->at(n));
			return true;
		}
	}
	return false;
}

bool FeComPar::setFrame(const QString & frame)
{
	int num, n;
	for (num = vecFrames->size(), n = 0; n < num; n++) {
		if (vecFrames->at(n)->text() == frame) {
			selectFrame(vecFrames->at(n));
			return true;
		}
	}
	return false;
}

void FeComPar::arrangeMyWidgets(void)
{
	int lblW = 150;
	int valW = 100;
	setGeometry(x(), y(), FECOMPAR_WIDTH, FECOMPAR_HEIGHT);
	btnSelCom->setGeometry(0, 0, 150, 20);
	btnOpenClose->setGeometry(FECOMPAR_WIDTH - 150, 0, 150, 20);

	frmCom->setGeometry(0, 25, FECOMPAR_WIDTH, FECOMPAR_HEIGHT - 25);
	lblCom->setGeometry(5, 5, 50, 20);
	lblSelComPort    ->setGeometry(5,  35, lblW, 20); btnSelComPort    ->setGeometry(lblW + 10,  35, valW, 20);
	lblSelBaudrate   ->setGeometry(5,  60, lblW, 20); btnSelBaudrate   ->setGeometry(lblW + 10,  60, valW, 20);
	lblSelFrame      ->setGeometry(5,  85, lblW, 20); btnSelFrame      ->setGeometry(lblW + 10,  85, valW, 20);
	lblBlkTimeout    ->setGeometry(5, 110, lblW, 20); sbxBlkTimeout    ->setGeometry(lblW + 10, 110, valW, 20);
	lblCharTimeoutMul->setGeometry(5, 135, lblW, 20); sbxCharTimeoutMul->setGeometry(lblW + 10, 135, valW, 20);
	lblTxTimeCtrl    ->setGeometry(5, 160, lblW, 20); sbxTxTimeCtrl    ->setGeometry(lblW + 10, 160, valW, 20);

	frmUsb->setGeometry(0, 25, FECOMPAR_WIDTH, FECOMPAR_HEIGHT - 25);
	lblUsb->setGeometry(5, 5, 50, 20);
	lblUsbTimeout->setGeometry(5, 35, lblW, 20); sbxUsbTimeout->setGeometry(lblW + 10, 35, valW, 20);

	frmTcp->setGeometry(0, 25, FECOMPAR_WIDTH, FECOMPAR_HEIGHT - 25);
	lblTcp->setGeometry(5, 5, 50, 20);
	lblIpAddr    ->setGeometry(5, 35, lblW, 20); editIpAddr   ->setGeometry(lblW + 10, 35, 200, 20);
	lblTcpPort   ->setGeometry(5, 60, lblW, 20); sbxTcpPort   ->setGeometry(lblW + 10, 60, valW, 20);
	lblTcpTimeout->setGeometry(5, 85, lblW, 20); sbxTcpTimeout->setGeometry(lblW + 10, 85, valW, 20);
}

void FeComPar::selectComPort(QAction *act)
{
	btnSelComPort->setText(act->text());
}

void FeComPar::selectBaudrate(QAction *act)
{
	btnSelBaudrate->setText(act->text());
}

void FeComPar::selectFrame(QAction *act)
{
	btnSelFrame->setText(act->text());
}

void FeComPar::dialogOpenClose(void)
{
	if (m_connectionHandle < 0) {
		QApplication::setOverrideCursor(Qt::WaitCursor);
		openConnection();
		QApplication::restoreOverrideCursor();
		if (m_connectionHandle < 0) {
			QString message = QString("Cannot open connection (error=%1)").arg(m_connectionHandle);
			QMessageBox::critical(this, "Open connection", message);
			return;
		}
		btnOpenClose->setText("Close connection");
	} else {
		closeConnection();
		btnOpenClose->setText("Open connection");
	}
}



/******************************************************************************************************/

FeComParDialog::FeComParDialog(QWidget *parent)
	: QDialog(parent)
{
	setWindowTitle("Communication parameter");
	comPar = new FeComPar(this);
	btnOk = new QPushButton(tr("Ok"), this);
    btnCancel = new QPushButton(tr("Cancel"), this);
	arrangeMyWidgets();
	connect(btnOk,     SIGNAL(clicked()), this, SLOT(accept()));
	connect(btnCancel, SIGNAL(clicked()), this, SLOT(reject()));
	connect(comPar, SIGNAL(connectionStateChanged()), this, SIGNAL(connectionStateChanged()));
}

FeComParDialog::~FeComParDialog()
{

}

int FeComParDialog::exec(void)
{
	int     oldComType = communicationType();
	QString oldComPort = comPort();
	QString oldBaudrate = baudrate();
	QString oldFrame = frame();
	int     oldBlkTimeout = blockTimeout();
	int     oldCharTimeoutMultiplier = charTimeoutMultiplier();
	int     oldTxTimeCtrl = txTimeCtrl();

	int     oldUsbTimeout = usbTimeout();

	QString oldIpAddr = ipAddr();
	int     oldTcpPort = tcpPort();
	int     oldTcpTimeout = tcpTimeout();

	if (QDialog::exec() == QDialog::Accepted)
		return QDialog::Accepted;

	setCommunicationType(oldComType);
	setComPort(oldComPort);
	setBaudrate(oldBaudrate);
	setFrame(oldFrame);
	setBlockTimeout(oldBlkTimeout);
	setCharTimeoutMultiplier(oldCharTimeoutMultiplier);
	setTxTimeCtrl(oldTxTimeCtrl);

	setUsbTimeout(oldUsbTimeout);

	setIpAddr(oldIpAddr);
	setTcpPort(oldTcpPort);
	setTcpTimeout(oldTcpTimeout);
	return QDialog::Rejected;
}

void FeComParDialog::arrangeMyWidgets(void)
{
	int h = comPar->height();
	int w = comPar->width();
	int totH = 5 + h + 5 + 20 + 5;
	int totW = 5 + w + 5;
	comPar   ->setGeometry( 5, 5, w, h);
	btnOk    ->setGeometry( 5, totH - 25, 60, 20);
	btnCancel->setGeometry(70, totH - 25, 60, 20);
	setFixedSize(totW, totH);
}
