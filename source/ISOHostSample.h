#ifndef ISOHOST_SAMPLE_H
#define ISOHOST_SAMPLE_H

#include <QMainWindow>
#include <QMenu>
#include <QFrame>
#include <QLabel>
#include <QFont>
#include <QPushButton>
#include <QTableWidget>
#include <QHeaderView>
#include <QLineEdit>
#include <QPlainTextEdit>
#include <QSpinBox>
#include <QCheckBox>
#include "feComPar.h"
#include "feHexEdit.h"
#include "feAuthentMifareDialog.h"
#include "ISOHostThread.h"



/* The ISOHostSample class handles the GUI of the sample application.
 *
 * Communication to the reader is handled by the ISOHostThread (see ISOHostThread.h).
 *
 * Additional GUI helper classes are:
 * - FeComParDialog        : For the communication settings, openning and closing the communication port.
 * - FeHexEdit             : Hex editor for input / display of data blocks
 * - FeAuthentMifareDialog : Input dialog for authentication key
 */
class ISOHostSample : public QMainWindow
{
	Q_OBJECT

public:
	ISOHostSample(QWidget *parent = NULL);
	~ISOHostSample();

private:
	static const int ColTagType = 0;
	static const int ColUid = 1;

	static const int BankReservedMem = 0;
	static const int BankEpcMem = 1;
	static const int BankTidMem = 2;
	static const int BankUserMem = 3;

	QFrame         *frmTagList;
	QLabel         *lblTagList;
	QLineEdit      *ledStatus;
	QTableWidget   *tabTagList;
	QPushButton    *btnComm;
	QPushButton    *btnStart;
	QPushButton    *btnStop;

	QFrame         *frmData;
	QLabel         *lblData;
	FeHexEdit      *hedData;
    QPushButton    *btnLoadData;
    QPushButton    *btnSaveData;
    QLabel         *lblFile;
	QPushButton    *btnReadData;
	QPushButton    *btnWriteData;
	QPushButton    *btnClearData;
	QLabel         *lblMemBank;
	QPushButton    *btnMemBank;
	QMenu          *menMemBank;
	QAction        *actMemRes;
	QAction        *actMemEpc;
	QAction        *actMemTid;
	QAction        *actMemUser;
	int             m_memBank;
	QLabel         *lblDbAddr;
	QSpinBox       *sbxDbAddr;
	QLabel         *lblDbNum;
	QSpinBox       *sbxDbNum;
	QLabel         *lblDbSize;
	QSpinBox       *sbxDbSize;

    std::vector<QStringList> csvList;
    QLabel         *lblCsvLin;
    QSpinBox       *sbxCsvLin;
    QLabel         *lblCsvCol;
    QSpinBox       *sbxCsvCol;

	QFrame         *frmProt;
	QLabel         *lblProt;
	QPlainTextEdit *tedProt;
	QPushButton    *btnClearProt;
	QCheckBox      *cbxEnabProt;

	FeComParDialog          *dlgComPar;
	FeAuthentMifareDialog   *dlgAuthMifare;
	ISOHostThread           *thdIsoHost;

	void            timerEvent(QTimerEvent *event);
	void            resizeEvent(QResizeEvent *event);
	void            arrangeMyWidgets(void);
	void            setTagListRowCount(int numRows);
	void            setStatusStarted(bool on);
	QString         uidOfSelectedTag(void);
	uint            handlerTypeOfSelectedTag(void);
    QString         getCsvSelection(void);
private slots:
	void            comPortDialog(void);
	void            startTagList(void);
	void            stopTagList(void);
	void            tagSelectionChanged(void);
	void            changeDataBlockSize(int size);
    void            changeCsvSelection(void);
    void            loadData(void);
    void            saveData(void);
	void            readData(void);
	void            writeData(void);
	void            clearData(void);
	void            setMemBank(QAction *act);
	void            clearProt(void);

	void            comError(const QString & errorText);
	void            newProtocol(const QString & prot);
	void            newTagList(void);
	void            dataRead(void);
	void            dataWritten(void);
};

#endif // ISOHOST_SAMPLE_H
