#include "feHexEdit.h"



FeHexBlock::FeHexBlock(unsigned int blockIndex, unsigned int bytesPerBlock, QWidget *parent)
	: QWidget(parent)
{
	QFont font("Courier New");
	lblBlock = new QLabel(QString("DB%1").arg(blockIndex, 4, 10, QChar(' ')), this);
	lblBlock->setFont(font);

	ledHex = new QLineEdit(this);
	ledHex->setFont(font);

	ledAsc = new QLineEdit(this);
	ledAsc->setFont(font);
	ledAsc->setReadOnly(true); // FIXME: allow editing here

	connect(ledHex, SIGNAL(textEdited(const QString &)), this, SLOT(hexTextEdited(const QString &)));

	setBytePerBlock(bytesPerBlock);
}

FeHexBlock::~FeHexBlock()
{

}

void FeHexBlock::setBytePerBlock(unsigned int bytesPerBlock)
{
	m_bytesPerBlock = bytesPerBlock;
	m_hexInputMask.clear();
	unsigned int n;
	for (n = 0; n < m_bytesPerBlock; n++)
		m_hexInputMask.append("HH ");
	ledHex->setInputMask(m_hexInputMask);

	m_ascInputMask.clear();
	for (n = 0; n < m_bytesPerBlock; n++)
		m_ascInputMask.append("X");
	ledAsc->setInputMask(m_ascInputMask);

	setBinary( QByteArray(m_bytesPerBlock, 0x00) );
	arrangeMyWidgets();
}

void FeHexBlock::setBinary(const QByteArray & bin)
{
	ledHex->setText( binToHexText(bin) );
	ledAsc->setText( binToAscText(bin) );
}

QByteArray FeHexBlock::binary(void)
{
	return hexTextToBin( ledHex->text() );
}

void FeHexBlock::arrangeMyWidgets(void)
{
	QFontMetrics fm(lblBlock->font());
	int lblW = fm.boundingRect(lblBlock->text()).width() + 5;
	int hexW = fm.boundingRect(m_hexInputMask).width() + 15;
	int ascW = fm.boundingRect(m_ascInputMask).width() + 10;
	int blkH = 20;

	int xChild = 0;
	lblBlock->setGeometry(xChild, 0, lblW, blkH); xChild += lblW;
	ledHex  ->setGeometry(xChild, 0, hexW, blkH); xChild += hexW;
	ledAsc  ->setGeometry(xChild, 0, ascW, blkH); xChild += ascW;
//	setFixedSize(xChild, blkH);
	setGeometry(x(), y(), xChild, blkH);
}

QByteArray FeHexBlock::hexTextToBin(const QString & hexText)
{
	QByteArray hexBA = hexText.toLatin1();
	return QByteArray::fromHex(hexBA);
}

QByteArray FeHexBlock::ascTextToBin(const QString & ascText)
{
	return ascText.toLatin1();
}

QString FeHexBlock::binToHexText(const QByteArray & bin)
{
	return QString( bin.toHex().toUpper() );
}

QString FeHexBlock::binToAscText(const QByteArray & bin)
{
	QString ascText;
	int num = bin.size();
	int n;
	for (n = 0; n < num; n++) {
		char c = bin.at(n);
		ascText.append( ((c >= 0x20) && ( c <= 0x7E)) ? c : '.' );
	}
	return ascText;
}

void FeHexBlock::hexTextEdited(const QString & text)
{
	ledAsc->setText( binToAscText(hexTextToBin(text)) );
}

void FeHexBlock::ascTextEdited(const QString & text) // FIXME: allow editing in ASCII line edit
{

}



FeHexEdit::FeHexEdit(unsigned int maxBlocks, unsigned int bytesPerBlock, QWidget *parent)
	: QScrollArea (parent)
{
	setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
	setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
	setBlocks(maxBlocks, bytesPerBlock);
	arrangeMyWidgets();
}

FeHexEdit::~FeHexEdit()
{
	clear();
}

void FeHexEdit::configure(unsigned int maxBlocks, unsigned int bytesPerBlock)
{
	clear();
	setBlocks(maxBlocks, bytesPerBlock);
	arrangeMyWidgets();
}

void FeHexEdit::resetBinaryData(void)
{
	unsigned int maxBlocks = m_blocks.size();
	QVector<unsigned char> data(maxBlocks * m_bytesPerBlock, 0x00);
	setBinaryData(data, 0, m_bytesPerBlock);
}

void FeHexEdit::setBinaryData(const QVector<unsigned char> & data, unsigned int firstBlock, unsigned int bytesPerBlock)
{
	unsigned int maxBlocks = m_blocks.size();
	if (bytesPerBlock != m_bytesPerBlock)
		configure(maxBlocks, bytesPerBlock);

	unsigned int n;
	unsigned int numBlocks = data.size() / bytesPerBlock;
	const char *cp = reinterpret_cast<const char *>(data.data());
	for (n = 0; (n < numBlocks) && (firstBlock + n < maxBlocks); n++) {
		QByteArray ba(cp, bytesPerBlock);
		m_blocks.at(firstBlock + n)->setBinary(ba);
		cp += bytesPerBlock;
	}
}

void FeHexEdit::getBinaryData(QVector<unsigned char> & data, unsigned int firstBlock, unsigned int numBlocks)
{
	data.clear();
	unsigned int maxBlocks = m_blocks.size();
	unsigned int n;
	for (n = 0; (n < numBlocks) && (firstBlock + n < maxBlocks); n++) {
		QByteArray ba = m_blocks.at(firstBlock + n)->binary();
		int baSz = ba.size();
		int i;
		for (i = 0; i < baSz; i++) {
			data.append(ba.at(i));
		}
	}
}

void FeHexEdit::clear()
{
	unsigned int maxBlocks = m_blocks.size();
	unsigned int n;
	for (n = 0; n < maxBlocks; n++) {
		delete m_blocks.at(n);
	}
	m_blocks.clear();
	m_bytesPerBlock = 0;
	QWidget *wdg = takeWidget();
	if (wdg != NULL)
		delete wdg;
}

void FeHexEdit::setBlocks(unsigned int maxBlocks, unsigned int bytesPerBlock)
{
	m_bytesPerBlock = bytesPerBlock;
	m_edit = new QWidget(this);

	unsigned int n;
	for (n = 0; n < maxBlocks; n++) {
		FeHexBlock *blk = new FeHexBlock(n, m_bytesPerBlock, m_edit);
		m_blocks.append(blk);
	}
	setWidget(m_edit);
}

void FeHexEdit::arrangeMyWidgets(void)
{
	unsigned int maxBlocks = m_blocks.size();
	if (maxBlocks == 0)
		return;

	int y = 0;
	int w = m_blocks.at(0)->width();
	int h = 0;
	unsigned int n;
	for (n = 0; n < maxBlocks; n++) {
		FeHexBlock *blk = m_blocks.at(n);
		h = blk->height();
		blk->setGeometry(0, y, w, h);
		y += h;
	}
	m_edit->setGeometry(0, 0, w, y);
}
