#include "ISOHostSample.h"
#include <QApplication>

int main(int argc, char *argv[])
{
	QApplication app(argc, argv);
#ifdef _MSC_VER
    app.setFont(QFont("Arial", 10));
#else
    app.setFont(QFont("Liberation Serif", 10));
#endif

    ISOHostSample mw;    // create the GUI object,
	mw.startTimer(100);  // activate the initial appearence of the COM settings dialog,
	mw.show();           // show the GUI
	return app.exec();   // and start it.
}
