#include "ISOHostSample.h"

#include <QFileDialog>
#include <QTextStream>
#include <QFile>

ISOHostSample::ISOHostSample(QWidget *parent)
	: QMainWindow(parent)
{
	setWindowTitle("ISOHostSample by FEIG ELECTRONIC GmbH   OBID i-scan - advanced reader technologies");

	/* tag list frame */
	frmTagList = new QFrame(this);
	frmTagList->setFrameStyle(QFrame::Panel | QFrame::Raised);
	frmTagList->setLineWidth(2);

	lblTagList = new QLabel("Tag List", frmTagList);
	lblTagList->setAlignment(Qt::AlignVCenter | Qt::AlignLeft);
	QFont boldFont = lblTagList->font();
	boldFont.setBold(true);
	lblTagList->setFont(boldFont);

	ledStatus = new QLineEdit(frmTagList);
	ledStatus->setReadOnly(true);

	tabTagList = new QTableWidget(0, 2, frmTagList);
	QStringList hdr;
	hdr.append("Tag Type");
	hdr.append("UID");
	tabTagList->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
	tabTagList->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
	tabTagList->setSelectionMode(QAbstractItemView::SingleSelection);
	tabTagList->setSelectionBehavior(QAbstractItemView::SelectRows);
	tabTagList->setHorizontalHeaderLabels(hdr);
	tabTagList->horizontalHeader()->setFixedHeight(25);
	tabTagList->verticalHeader()->hide();
	tabTagList->setColumnWidth(ColTagType, 100);
	tabTagList->setColumnWidth(ColUid,     200);
	setTagListRowCount(20);

	btnComm  = new QPushButton("Comm. port", frmTagList);
	btnStart = new QPushButton("Start", frmTagList);
	btnStop  = new QPushButton("Stop", frmTagList);

	/* data frame */
	frmData = new QFrame(this);
	frmData->setFrameStyle(QFrame::Panel | QFrame::Raised);
	frmData->setLineWidth(2);

	lblData = new QLabel("Read-Write Multiple Blocks", frmData);
	lblData->setAlignment(Qt::AlignVCenter | Qt::AlignLeft);
	lblData->setFont(boldFont);

	hedData = new FeHexEdit(64, 4, frmData);

    btnLoadData  = new QPushButton("Load",  frmData);
    btnSaveData  = new QPushButton("Save",  frmData);

    lblFile   = new QLabel("", frmData);


	btnReadData  = new QPushButton("Read",  frmData);
	btnWriteData = new QPushButton("Write", frmData);
	btnClearData = new QPushButton("Clear", frmData);

	lblMemBank = new QLabel("Bank", frmData);
	lblMemBank->setAlignment(Qt::AlignBottom | Qt::AlignLeft);

	btnMemBank = new QPushButton(frmData);
	menMemBank = new QMenu(this);
	actMemRes  = menMemBank->addAction("Reserved"); actMemRes ->setData(QVariant(BankReservedMem));
	actMemEpc  = menMemBank->addAction("EPC");      actMemEpc ->setData(QVariant(BankEpcMem));
	actMemTid  = menMemBank->addAction("TID");      actMemTid ->setData(QVariant(BankTidMem));
	actMemUser = menMemBank->addAction("User");     actMemUser->setData(QVariant(BankUserMem));
	btnMemBank->setMenu(menMemBank);
	btnMemBank->setEnabled(false);

	lblDbAddr = new QLabel("DB-Addr", frmData);
	lblDbAddr->setAlignment(Qt::AlignVCenter | Qt::AlignLeft);

	sbxDbAddr = new QSpinBox(frmData);
	sbxDbAddr->setRange(0, INT_MAX);
	sbxDbAddr->setValue(0);

	lblDbNum = new QLabel("DB-Num", frmData);
	lblDbNum->setAlignment(Qt::AlignVCenter | Qt::AlignLeft);

	sbxDbNum = new QSpinBox(frmData);
	sbxDbNum->setRange(0, INT_MAX);
	sbxDbNum->setValue(4);

	lblDbSize = new QLabel("DB-Size", frmData);
	lblDbSize->setAlignment(Qt::AlignVCenter | Qt::AlignLeft);

	sbxDbSize = new QSpinBox(frmData);
	sbxDbSize->setRange(0, INT_MAX);
	sbxDbSize->setValue(4);

    // CSV
    lblCsvLin= new QLabel("CSV Lin", frmData);
    lblCsvLin->setAlignment(Qt::AlignVCenter | Qt::AlignLeft);

    sbxCsvLin= new QSpinBox(frmData);
    sbxCsvLin->setRange(0, INT_MAX);
    sbxCsvLin->setValue(0);
    sbxCsvLin->setEnabled(false);

    lblCsvCol= new QLabel("CSV Col", frmData);
    lblCsvCol->setAlignment(Qt::AlignVCenter | Qt::AlignLeft);

    sbxCsvCol= new QSpinBox(frmData);
    sbxCsvCol->setRange(0, INT_MAX);
    sbxCsvCol->setValue(0);
    sbxCsvCol->setEnabled(false);

	/* protocol frame */
	frmProt = new QFrame(this);
	frmProt->setFrameStyle(QFrame::Panel | QFrame::Raised);
	frmProt->setLineWidth(2);

    lblProt = new QLabel("Protocol Window", frmProt);
	lblProt->setAlignment(Qt::AlignVCenter | Qt::AlignLeft);
	lblProt->setFont(boldFont);

	tedProt = new QPlainTextEdit(frmProt);
	tedProt->setFont(QFont("Courier New"));
	tedProt->setCenterOnScroll(false);
	tedProt->setReadOnly(true);
	tedProt->appendPlainText("Supported transponder types:\n"
	                         "- EPC Class 1 Generation 2\n"
							 "- NXP ICode SLi\n"
                             "- ISO 14443-3 MIFARE Classic\n"
                             "- NXP ICode Slix\n"
                             "- NXP ICode Slix2\n");

	btnClearProt = new QPushButton("Clear", frmProt);
	cbxEnabProt = new QCheckBox("Enable", frmProt);
	cbxEnabProt->setChecked(true);


	dlgComPar = new FeComParDialog(this);
    dlgComPar->setCommunicationType(FeComPar::FeUsb);

	dlgAuthMifare = new FeAuthentMifareDialog(0, "FFFFFFFFFFFF", 'A', this);

	thdIsoHost = new ISOHostThread(dlgComPar, this);

	connect(btnComm,      SIGNAL(clicked()),              this, SLOT(comPortDialog()));
	connect(btnStart,     SIGNAL(clicked()),              this, SLOT(startTagList()));
	connect(btnStop,      SIGNAL(clicked()),              this, SLOT(stopTagList()));
	connect(tabTagList,   SIGNAL(itemSelectionChanged()), this, SLOT(tagSelectionChanged()));
	connect(sbxDbSize,    SIGNAL(valueChanged(int)),      this, SLOT(changeDataBlockSize(int)));

    connect(sbxCsvLin,    SIGNAL(valueChanged(int)),      this, SLOT(changeCsvSelection()));
    connect(sbxCsvCol,    SIGNAL(valueChanged(int)),      this, SLOT(changeCsvSelection()));

    connect(btnLoadData,  SIGNAL(clicked()),              this, SLOT(loadData()));
    connect(btnSaveData,  SIGNAL(clicked()),              this, SLOT(saveData()));
    connect(btnReadData,  SIGNAL(clicked()),              this, SLOT(readData()));
	connect(btnWriteData, SIGNAL(clicked()),              this, SLOT(writeData()));
	connect(btnClearData, SIGNAL(clicked()),              this, SLOT(clearData()));
	connect(btnClearProt, SIGNAL(clicked()),              this, SLOT(clearProt()));

	connect(menMemBank, SIGNAL(triggered(QAction*)), this, SLOT(setMemBank(QAction*)));
	setMemBank(actMemRes);

	connect(thdIsoHost, SIGNAL(comError(const QString &)),    this, SLOT(comError(const QString &)),    Qt::QueuedConnection);
	connect(thdIsoHost, SIGNAL(newProtocol(const QString &)), this, SLOT(newProtocol(const QString &)), Qt::QueuedConnection);
	connect(thdIsoHost, SIGNAL(newTagList()),                 this, SLOT(newTagList()),                 Qt::QueuedConnection);
	connect(thdIsoHost, SIGNAL(dataRead()),                   this, SLOT(dataRead()),                   Qt::QueuedConnection);
	connect(thdIsoHost, SIGNAL(dataWritten()),                this, SLOT(dataWritten()),                Qt::QueuedConnection);

	setGeometry(200, 200, 1200, 600);
	setStatusStarted(false);
}

ISOHostSample::~ISOHostSample()
{
}


void ISOHostSample::timerEvent(QTimerEvent *event)
{
	killTimer(event->timerId());
	comPortDialog();
}

void ISOHostSample::resizeEvent(QResizeEvent *event)
{
	arrangeMyWidgets();
}

void ISOHostSample::arrangeMyWidgets(void)
{
	int sep = 5;
	int w = width();
	int h = height();
	int protH = (h - 3*sep) / 2;
	int protW = w - 2*sep;
	int tagH  = protH;
	int tagW  = (w - 3*sep) / 2;
	int dataH = tagH;
	int dataW = (w - 3*sep) / 2;
    int btnW = 100;
	int xBtn;
	int lblW;

	frmData->setGeometry(sep, sep, dataW, dataH);
	lblData->setGeometry(sep, sep, dataW-10, 20);
	xBtn = dataW-btnW-sep;
	hedData->setGeometry(sep, 35, xBtn-2*sep, dataH-sep-35);
    lblFile ->setGeometry(xBtn,  35, 100, 20);
    btnLoadData ->setGeometry(xBtn,  60, btnW, 20);
    btnSaveData ->setGeometry(xBtn,  85, btnW, 20);
    btnReadData ->setGeometry(xBtn,  110, btnW, 20);
    btnWriteData->setGeometry(xBtn,  135, btnW, 20);
    btnClearData->setGeometry(xBtn,  160, btnW, 20);
    lblMemBank  ->setGeometry(xBtn, 225, btnW, 20);
    btnMemBank  ->setGeometry(xBtn, 250, btnW, 20);
    lblW = 60;
    lblDbAddr   ->setGeometry(xBtn, 275, lblW, 20); sbxDbAddr->setGeometry(xBtn+lblW, 275, btnW-lblW, 20);
    lblDbNum    ->setGeometry(xBtn, 300, lblW, 20); sbxDbNum ->setGeometry(xBtn+lblW, 300, btnW-lblW, 20);
    lblDbSize   ->setGeometry(xBtn, 325, lblW, 20); sbxDbSize->setGeometry(xBtn+lblW, 325, btnW-lblW, 20);

    lblCsvLin   ->setGeometry(xBtn, 360, lblW, 20); sbxCsvLin ->setGeometry(xBtn+lblW, 360, btnW-lblW, 20);
    lblCsvCol   ->setGeometry(xBtn, 385, lblW, 20); sbxCsvCol->setGeometry(xBtn+lblW, 385, btnW-lblW, 20);


	frmTagList->setGeometry(sep+dataW+sep, sep, tagW, tagH);
	lblTagList->setGeometry(sep, sep, tagW-10, 20);
	xBtn = tagW-btnW-sep;
	ledStatus ->setGeometry(sep,  35, xBtn-2*sep, 20);
	tabTagList->setGeometry(sep,  60, xBtn-2*sep, tagH-sep-60);
	btnComm ->setGeometry(xBtn, 35, btnW, 20);
	btnStart->setGeometry(xBtn, 60, btnW, 20);
	btnStop ->setGeometry(xBtn, 85, btnW, 20);

	frmProt->setGeometry(sep, sep+tagH+sep, protW, protH);
	lblProt->setGeometry(sep, sep, protW-10, 20);
	xBtn = protW-btnW-sep;
	tedProt->setGeometry(sep, 35, xBtn-2*sep, protH-sep-35);
	btnClearProt->setGeometry(xBtn, 35, btnW, 20);
	cbxEnabProt ->setGeometry(xBtn, 70, btnW, 20);
}

void ISOHostSample::setTagListRowCount(int numRows)
{
	int n;
	tabTagList->setRowCount(numRows);
	for (n = 0; n < numRows; n++) {
		tabTagList->setRowHeight(n, 20);
	}
}

void ISOHostSample::setStatusStarted(bool on)
{
	btnComm->setDisabled(on);
	btnStart->setDisabled(on);
	btnStop->setDisabled(!on);
    btnLoadData->setDisabled(on);
    btnSaveData->setDisabled(on);
	btnReadData->setDisabled(on);
	btnWriteData->setDisabled(on);
}

QString ISOHostSample::uidOfSelectedTag(void)
{
	int row = tabTagList->currentRow();
	if ((row >= 0) && (row < tabTagList->rowCount()))
		return tabTagList->item(row, ColUid)->text();
	else
		return QString();
}

uint ISOHostSample::handlerTypeOfSelectedTag(void)
{
	int row = tabTagList->currentRow();
	if ((row >= 0) && (row < tabTagList->rowCount()))
		return tabTagList->item(row, ColTagType)->data(Qt::UserRole).toUInt();
	else
		return 0;
}

void ISOHostSample::comPortDialog(void)
{
	dlgComPar->showOpenCloseButton(true);
	dlgComPar->exec();
}

void ISOHostSample::startTagList(void)
{
	ledStatus->clear();
	thdIsoHost->startTagList();
	setStatusStarted(true);
}

void ISOHostSample::stopTagList(void)
{
	thdIsoHost->stopTagList();
	setStatusStarted(false);
}

void ISOHostSample::tagSelectionChanged(void)
{
}

void ISOHostSample::changeDataBlockSize(int size)
{
	unsigned int maxBlocks = hedData->maxBlocks();
	hedData->configure(maxBlocks, size);
}

void ISOHostSample::loadData(void)
{
    QString filelink = QFileDialog::getOpenFileName(this,
        tr("Open datas ..."),
        "../../datas/",
        tr("Datas (*.txt *.hex *.csv)") //tr("All Files (*)")
    );

    if(filelink.isEmpty()) {
        return;
    }

    QFileInfo fileInfo(filelink);

    // Et avec ce fileInfo, tu peux obtenir pleins de choses :
    QDir dir = fileInfo.dir(); // Dossier du fichier
    QString dirPath = fileInfo.filePath(); // Path vers le fichier
    QString fileName = fileInfo.fileName(); // Nom du fichier (sans le path)
    QString extension = fileInfo.suffix();
    // ...
    lblFile->setText(fileName);
    sbxCsvLin->setEnabled(false);
    sbxCsvCol->setEnabled(false);

    QFile file(dirPath);
    if(!file.open(QIODevice::ReadOnly)) {
        QMessageBox::information(this, tr("Unable to read file"), file.errorString());
    }

    QTextStream in(&file);


    QVector<unsigned char> datas;

    if(extension == "hex") {
        while(!in.atEnd()) {
            QString line = in.readLine();

            QByteArray values = QByteArray::fromHex(line.toLatin1());

            for(auto& value : values) {
                datas.push_back(static_cast<unsigned char>(value));
            }
        }
    }
    else if(extension == "txt") {
        while(!in.atEnd()) {
            QString line = in.readLine();

            QByteArray values = QByteArray::fromStdString(line.toStdString());

            for(auto& value : values) {
                datas.push_back(static_cast<unsigned char>(value));
            }
        }
    }
    else if(extension == "csv") {
        sbxCsvLin->setEnabled(true);
        sbxCsvCol->setEnabled(true);
        csvList.clear();
        while(!in.atEnd()) {
            QString line = in.readLine();
            QStringList wordList = line.split(';');
            csvList.push_back(wordList);
        }

        QString select = getCsvSelection();
        if(select != "") {
            QByteArray values = QByteArray::fromStdString(select.toStdString());
            for(auto& value : values) {
                datas.push_back(static_cast<unsigned char>(value));
            }
        }
    }

    hedData->setBinaryData(datas, sbxDbAddr->value(), hedData->bytesPerBlock());

    file.close();

    ledStatus->setText("Data block(s) load");
}

QString ISOHostSample::getCsvSelection(void) {
    QString select = "";
    try {
        if(sbxCsvLin->value() < csvList.size()) {
            if(sbxCsvCol->value() < csvList.at(sbxCsvLin->value()).size()) {
            select = csvList
                    .at(sbxCsvLin->value())
                    .at(sbxCsvCol->value())
            ;
            } else
                ledStatus->setText("Overflow csv col");
        } else
            ledStatus->setText("Overflow csv lin");
    } catch(const std::exception& exc) {
        ledStatus->setText("Exception overflow csv " + QString(exc.what()));
    }
    return select;
}

void ISOHostSample::changeCsvSelection(void) {
    QString select = getCsvSelection();
    if(select != "") {
        clearData();
        QByteArray values = QByteArray::fromStdString(select.toStdString());
        QVector<unsigned char> datas;
        for(auto& value : values) {
            datas.push_back(static_cast<unsigned char>(value));
        }
        hedData->setBinaryData(datas, sbxDbAddr->value(), hedData->bytesPerBlock());

        ledStatus->setText("Csv selection is changed");
    }
}

void ISOHostSample::saveData(void)
{
    QString filelink = QFileDialog::getSaveFileName(this,
            tr("Save data"), "../../datas/",
            tr("Datas (*.txt *.hex *.csv)")
    );

    if (filelink.isEmpty())
        return;

    QFileInfo fileInfo(filelink);

    // Et avec ce fileInfo, tu peux obtenir pleins de choses :
    QDir dir = fileInfo.dir(); // Dossier du fichier
    QString dirPath = fileInfo.filePath(); // Path vers le fichier
    QString fileName = fileInfo.fileName(); // Nom du fichier (sans le path)
    QString extension = fileInfo.suffix();

    //QString format = cbFormatType->currentText();

    QFile file(filelink);
    if (!file.open(QIODevice::WriteOnly)) {
        QMessageBox::information(this, tr("Unable to write file"),
            file.errorString());
        return;
    }

    QVector<unsigned char> datas;
    hedData->getBinaryData(datas, sbxDbAddr->value(), sbxDbNum ->value());

    QTextStream out(&file);

    sbxDbSize->value();
    int i = 0;
    for(auto& data : datas) {

        if(extension == "hex") {
            QString hexadecimal;
            hexadecimal = hexadecimal.setNum(static_cast<int>(data),16).toUpper();
            if(hexadecimal.size() == 1)
                out << "0";
            out << hexadecimal;
            i++;
            if(i % sbxDbSize->value() == 0 )
                out << "\r\n";
            else
                out << " ";

        } else if(extension == "txt") {

            out << QString::fromLatin1(reinterpret_cast<const char*>(&data), 1);

        } else if(extension == "csv") {

        }
    }

    ledStatus->setText("Data block(s) save");
}

void ISOHostSample::readData(void)
{
	ledStatus->clear();
	ISOHostTagDataBlock db;
	db.m_handlerType = handlerTypeOfSelectedTag();
	db.m_uid         = uidOfSelectedTag();
	db.m_bank        = m_memBank;
	db.m_dbAddr = sbxDbAddr->value();
	db.m_dbNum  = sbxDbNum ->value();
	db.m_dbSize = sbxDbSize->value();
	// we don't need a data vector here
	if (db.m_handlerType == TagHandler::TH_Base::TYPE_ISO14443) {
		dlgAuthMifare->setBlockAddr(db.m_dbAddr);

		if (dlgAuthMifare->exec() == QDialog::Rejected)
			return;

		db.m_key     = dlgAuthMifare->key();
		db.m_keyType = (dlgAuthMifare->keyType() == 'A') ? 0 : 1;
	}
	thdIsoHost->startReadData(db);
}

void ISOHostSample::writeData(void)
{
	ledStatus->clear();
	ISOHostTagDataBlock db;
	db.m_handlerType = handlerTypeOfSelectedTag();
	db.m_uid         = uidOfSelectedTag();
	db.m_bank        = m_memBank;
	db.m_dbAddr = sbxDbAddr->value();
	db.m_dbNum  = sbxDbNum ->value();
	db.m_dbSize = hedData->bytesPerBlock();
	hedData->getBinaryData(db, db.m_dbAddr, db.m_dbNum);
    if (db.m_handlerType == TagHandler::TH_Base::TYPE_ISO14443) {
		dlgAuthMifare->setBlockAddr(db.m_dbAddr);

		if (dlgAuthMifare->exec() == QDialog::Rejected)
			return;

		db.m_key     = dlgAuthMifare->key();
		db.m_keyType = (dlgAuthMifare->keyType() == 'A') ? 0 : 1;
	}
	thdIsoHost->startWriteData(db);
}

void ISOHostSample::clearData(void)
{
	hedData->resetBinaryData();
}

void ISOHostSample::setMemBank(QAction *act)
{
	btnMemBank->setText(act->text());
	m_memBank = act->data().toInt();
}

void ISOHostSample::clearProt(void)
{
	tedProt->clear();
}

void ISOHostSample::comError(const QString & errorText)
{
	ledStatus->setText(errorText);
}

void ISOHostSample::newProtocol(const QString & prot)
{
	if (cbxEnabProt->isChecked())
		tedProt->appendPlainText(prot);
}

void ISOHostSample::newTagList(void)
{
	btnMemBank->setEnabled(false);

	ISOHostTagList tagList;
	thdIsoHost->getTagList(tagList);
	int num = tagList.size();
	tabTagList->clearContents();
	tabTagList->setRowCount(num);
	int n;
	for (n = 0; n < num; n++) {
		tabTagList->setRowHeight(n, 20);
		const ISOHostTag & tag = tagList.at(n);
		unsigned int handlerType = tag.m_handlerType;

		QTableWidgetItem *item = new QTableWidgetItem( QString("0x%1").arg(tag.m_driverType, 2, 16, QChar('0')).toUpper() );
		item->setData(Qt::UserRole, QVariant(handlerType));
		tabTagList->setItem(n, ColTagType, item);

		item = new QTableWidgetItem( tag.m_uid );
		tabTagList->setItem(n, ColUid, item);

		if (handlerType == TagHandler::TH_Base::TYPE_EPC_CLASS1_GEN2)
			btnMemBank->setEnabled(true); // these tags have banks
	}
}

void ISOHostSample::dataRead(void)
{
	ISOHostTagDataBlock db;
	thdIsoHost->getTagDataBlock(db);
	sbxDbSize->setValue(db.m_dbSize);
	hedData->setBinaryData(db, db.m_dbAddr, db.m_dbSize);
	ledStatus->setText("Data block(s) read");
}

void ISOHostSample::dataWritten(void)
{
	ledStatus->setText("Data block(s) written");
}
