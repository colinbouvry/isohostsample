#ifndef FE_AUTHENT_MIFARE_DIALOG_H
#define FE_AUTHENT_MIFARE_DIALOG_H

#include <QDialog>
#include <QLabel>
#include <QPushButton>
#include <QLineEdit>



class FeAuthentMifareDialog : public QDialog
{
	Q_OBJECT

public:
	FeAuthentMifareDialog(unsigned int blockAddr, const QString & key, char keyType, QWidget *parent = NULL);
	~FeAuthentMifareDialog();

	void    setBlockAddr(unsigned int addr)   { m_blockAddr = addr; }
	void    setKey      (const QString & key) { ledKey->setText(key); }
	void    setKeyType  (char keyType)        { btnKeyType->setChecked( keyType == 'B' ); }

	QString key    (void) { return ledKey->text().replace(QChar(' '), QString()).toUpper(); }
	char    keyType(void) { return btnKeyType->isChecked() ? 'B' : 'A'; }

	int     exec(void);

private:
	unsigned int           m_blockAddr;
	QLabel                *lblAuth;
	QLineEdit             *ledKey;
	QPushButton           *btnKeyType;
	QPushButton           *btnOk;
	QPushButton           *btnCancel;

	void      arrangeMyWidgets(void);

private slots:
	void      keyTypeToggled(bool chkd) { btnKeyType->setText( chkd ? "Key Type B" : "Key Type A" ); }
};

#endif // FE_AUTHENT_MIFARE_DIALOG_H
