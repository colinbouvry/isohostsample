#include "feAuthentMifareDialog.h"



FeAuthentMifareDialog::FeAuthentMifareDialog(unsigned int blockAddr, const QString & key, char keyType, QWidget *parent)
	: QDialog(parent)
{
	setWindowTitle("Authentication for MIFARE");
	m_blockAddr = blockAddr;
	lblAuth = new QLabel(this);

	int val = 0xFFFFFFFFFFFF;
	bool ok;
	ledKey = new QLineEdit(this);
	ledKey->setFont(QFont("Courier New"));
	ledKey->setInputMask("HH HH HH HH HH HH");
	ledKey->setText(key);

	btnKeyType = new QPushButton(this);
	btnKeyType->setCheckable(true);
	connect(btnKeyType, SIGNAL(toggled(bool)), this, SLOT(keyTypeToggled(bool)));
	btnKeyType->setChecked(true);
	btnKeyType->setChecked(false);

	btnOk     = new QPushButton(tr("Ok"), this);
    btnCancel = new QPushButton(tr("Cancel"), this);

	arrangeMyWidgets();
	connect(btnOk,      SIGNAL(clicked()),     this, SLOT(accept()));
	connect(btnCancel,  SIGNAL(clicked()),     this, SLOT(reject()));
}

FeAuthentMifareDialog::~FeAuthentMifareDialog()
{

}

int FeAuthentMifareDialog::exec(void)
{
	lblAuth->setText( QString("Insert key for block address %1").arg(m_blockAddr) );
	return QDialog::exec();
}

void FeAuthentMifareDialog::arrangeMyWidgets(void)
{
	int maxW = 300;
	lblAuth   ->setGeometry(5,  5, maxW-10, 20);
	ledKey    ->setGeometry(5, 30, 130, 20);
	btnKeyType->setGeometry(5, 55, 130, 20);

	btnOk->setGeometry(5, 100, 60, 20); btnCancel->setGeometry(70, 100, 60, 20);
	setFixedSize(maxW, 125);
}
