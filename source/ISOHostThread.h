#ifndef ISOHOST_THREAD_H
#define ISOHOST_THREAD_H

#define _AFXDLL

#include <QThread>
#include <QMutex>
#include <QVector>
#include <iostream>
#include "FedmIscCoreApi.h"
#include "feComPar.h"

using namespace FEDM::Core;



class ISOHostTag
{
public:
	ISOHostTag();
	~ISOHostTag();

	ISOHostTag &operator=  (const ISOHostTag & other);
	bool        operator== (const ISOHostTag & other) const;

	uint    m_driverType;
	uint    m_handlerType;
	QString m_uid;
};

class ISOHostTagList : public QVector<ISOHostTag> { };



class ISOHostTagDataBlock : public QVector<unsigned char>
{
public:
	ISOHostTagDataBlock();
	ISOHostTagDataBlock(uint dbAddr, uint dbNum, uint dbSize);
	~ISOHostTagDataBlock() {}

	QString    toString(void) const;

	ISOHostTagDataBlock &operator=(const ISOHostTagDataBlock & other);

	uint    m_handlerType;
	QString m_uid;
	QString m_key;
	uint    m_keyType;

	uint m_bank;
	uint m_dbAddr;
	uint m_dbNum;
	uint m_dbSize;
};



/* The thread object handles the communication with the reader via the ReaderModule m_reader.
 * 
 * Here we support 3 funtions:
 * 1. Query the tag list continously (controlled by startTagList() and stopTagList()).
 *    Each new tag list is signaled by newTagList() and can be copied to the main thread with getTagList().
 * 2. Read data blocks from the tag once (triggered by startReadData()).
 *    When dataRead() is send the data can be copied with getTagDataBlock().
 * 3. Write data blocks once (triggered by startWriteData()).
 *    On success dataWritten() is send.
 * Data access via getTagList() and getTagDataBlock() is mutex controlled.
 *
 * Failures are signaled with comError(). Each protocol (send and receive) is signaled with newProtocol(). This is
 * triggered by the LogListener via function OnNewReaderProtocol().
 *
 * The data blocks from and to tags are transmitted in the reader protocols with MSB first. In the data buffers
 * for the ReadMultipleBlocks() and WriteMultipleBlocks() functions of the tag handler classes these block data
 * is stored with LSB first.
 */
class ISOHostThread : public QThread, ILogListener
{
	Q_OBJECT

public:
	ISOHostThread(FeComParDialog *dlgComPar, QObject *parent = NULL);
	~ISOHostThread();

	void getTagList(ISOHostTagList & list);
	void getTagDataBlock(ISOHostTagDataBlock & db);

public slots:
	void startTagList(void);
	void stopTagList(void);
	void startReadData (const ISOHostTagDataBlock & db);
	void startWriteData(const ISOHostTagDataBlock & db);

signals:
	void comError(const QString & errorText);
	void newProtocol(const QString & prot);
	void newTagList(void);
	void dataRead(void);
	void dataWritten(void);

protected:
	static const int     MaxBufSize = 4096;

	FeComParDialog      *m_dlgComPar;
	ReaderModule         m_reader;
	bool                 m_isError;
	QString              m_errorText;
	QMutex               m_mtxTagList;
	QMutex               m_mtxDB;
	ISOHostTagDataBlock  m_db;

	bool m_doTagList;
	bool m_doReadDB;
	bool m_doWriteDB;

	bool openConnection(void);
	void closeConnection(void);
	void setErrorText(int errorCode);
	void setErrorText(const QString & errorText);

	void run(void);

	void OnNewReaderProtocol(unsigned int uiDirection, std::string sLogText);
};

#endif // ISOHOST_THREAD_H
