#include "ISOHostThread.h"



ISOHostTag::ISOHostTag()
{
	m_driverType = m_handlerType = 0;
}

ISOHostTag::~ISOHostTag()
{

}

ISOHostTag &ISOHostTag::operator=(const ISOHostTag & other)
{
	m_driverType  = other.m_driverType;
	m_handlerType = other.m_handlerType;
	m_uid  = other.m_uid;
	return *this;
}

bool ISOHostTag::operator==(const ISOHostTag & other) const
{
	return (m_driverType  == other.m_driverType ) &&
		   (m_handlerType == other.m_handlerType) &&
		   (m_uid         == other.m_uid);
}



/**** Data Block ****/

ISOHostTagDataBlock::ISOHostTagDataBlock()
	: QVector<unsigned char>()
{
	m_handlerType = 0;
	m_key = "FFFFFFFFFFFF";
	m_keyType = 0;
	m_bank = 0;
	m_dbAddr = m_dbNum = m_dbSize = 0;
}

ISOHostTagDataBlock::ISOHostTagDataBlock(uint dbAddr, uint dbNum, uint dbSize)
	: QVector<unsigned char>(dbNum * dbSize, 0x00)
{
	m_handlerType = 0;
	m_key = "FFFFFFFFFFFF";
	m_keyType = 0;
	m_bank = 0;
	m_dbAddr = dbAddr;
	m_dbNum  = dbNum;
	m_dbSize = dbSize;
}

QString ISOHostTagDataBlock::toString(void) const
{
	QString str;
	uint blkIdx, byteIdx;

	for (blkIdx = 0; blkIdx < m_dbNum; blkIdx++) {
		str.append(QString("DB%1:").arg(m_dbAddr + blkIdx, 3, 10, QChar(' ')));
		for (byteIdx = 0; byteIdx < m_dbSize; byteIdx++) {
			unsigned byte = at(blkIdx * m_dbSize + byteIdx);
			str.append(QString(" %1").arg(byte, 2, 16, QChar('0')));
		}
		str.append('\n');
	}

	return str;
}

ISOHostTagDataBlock &ISOHostTagDataBlock::operator=(const ISOHostTagDataBlock & other)
{
	m_handlerType = other.m_handlerType;
	m_uid         = other.m_uid;
	m_key         = other.m_key;
	m_keyType     = other.m_keyType;
	m_bank        = other.m_bank;
	m_dbAddr      = other.m_dbAddr;
	m_dbNum       = other.m_dbNum;
	m_dbSize      = other.m_dbSize;
	QVector::operator=(other);
	return *this;
}



/************************************************************* The Thread ******************************************************/

ISOHostThread::ISOHostThread(FeComParDialog *dlgComPar, QObject *parent) : QThread(parent)
{
	m_dlgComPar = dlgComPar;
	m_doTagList = m_doReadDB = m_doWriteDB = false;
    m_reader.ILog.AddProtocolEventListener(this, ILogGroup::EVENT_ID_TRANSCEIVE_AS_STRING);
	m_reader.IPort.SetProtocolFrameSupport(IPortGroup::PROTOCOL_FRAME_ADVANCED);
	m_reader.IHmTable.SetSize(256);
}

ISOHostThread::~ISOHostThread()
{

}

void ISOHostThread::getTagList(ISOHostTagList & list)
{
	list.clear();
	ISOHostTag tag;

	m_mtxTagList.lock();
	const ITagGroup::TH_LIST & tl = m_reader.ITag.GetTagList();
	ITagGroup::TH_LIST_ITOR itor = tl.cbegin();
	while (itor != tl.cend()) {
		tag.m_uid = QString::fromStdString( itor->first );
		tag.m_driverType  = itor->second->GetTagDriverType();
		tag.m_handlerType = itor->second->GetTagHandlerType();
		list.append(tag);
		itor++;
	}
	m_mtxTagList.unlock();
}

void ISOHostThread::getTagDataBlock(ISOHostTagDataBlock & db)
{
	m_mtxDB.lock();
	db = m_db;
	m_mtxDB.unlock();
}

void ISOHostThread::startTagList(void)
{
	m_doTagList = true;
	m_doReadDB = m_doWriteDB = false;
	start();
}

void ISOHostThread::stopTagList(void)
{
	m_doTagList = m_doReadDB = m_doWriteDB = false;
}

void ISOHostThread::startReadData(const ISOHostTagDataBlock & db)
{
	m_mtxDB.lock();
	m_db = db;
	m_mtxDB.unlock();
	m_doReadDB = true;
	m_doTagList = m_doWriteDB = false;
	start();
}

void ISOHostThread::startWriteData(const ISOHostTagDataBlock & db)
{
	m_mtxDB.lock();
	m_db = db;
	m_mtxDB.unlock();
	m_doWriteDB = true;
	m_doTagList = m_doReadDB = false;
	start();
}


bool ISOHostThread::openConnection(void)
{
	int hnd = m_dlgComPar->openConnection();
    if (FeComPar::isConnection(hnd)) {
		m_reader.IPort.SetPortHnd(hnd);
		m_reader.IInfo.ReadReaderInfo();
		return true;
	} else {
		m_reader.IPort.SetPortHnd(0);
		setErrorText("Open connection failed");
		return false;
	}
}

void ISOHostThread::closeConnection(void)
{
	m_dlgComPar->closeConnection();
	m_reader.IPort.SetPortHnd(0);
}

void ISOHostThread::setErrorText(int errorCode)
{
	m_isError = (errorCode != 0);
	if (errorCode < 0)
		m_errorText = QString::fromStdString( ErrorCode::GetErrorText(errorCode) );
	else
		m_errorText = QString("Reader status 0x%1").arg(errorCode, 2, 16, QChar('0'));
	emit comError(m_errorText);
}

void ISOHostThread::setErrorText(const QString & errorText)
{
	m_isError = true;
	m_errorText = errorText;
	emit comError(m_errorText);
}


void ISOHostThread::run(void)
{
	int rc = 0;
	m_isError = false;
	m_errorText = "";

	if (!openConnection())
		return;

	/* reading tag list continuosly */
	while (m_doTagList) {
		m_mtxTagList.lock();
		rc = m_reader.ITag.Inventory();
		m_mtxTagList.unlock();

		if (rc == 0)
			emit newTagList();
		else
			setErrorText(rc);

		msleep(1000);
	}

	/* read a data block once */
	if (m_doReadDB) {
		TagHandler::TH_Base *tagHandler = m_reader.ITag.GetTagHandler( m_db.m_uid.toStdString() );
        std::cout<< "handlerType : " << m_db.m_handlerType << std::endl;
		switch (m_db.m_handlerType) {
            case TagHandler::TH_Base::TYPE_EPC_CLASS1_GEN2:
				{
					TagHandler::TH_EPC_Class1_Gen2 *tagEpcC1G2 = dynamic_cast<TagHandler::TH_EPC_Class1_Gen2 *>(tagHandler);
					if (tagEpcC1G2 == NULL) {
						setErrorText("Fatal tag handler error");
						break;
					}

					m_mtxDB.lock();
					m_db.resize(MaxBufSize);
					rc = tagEpcC1G2->ReadMultipleBlocks(m_db.m_bank, m_db.m_dbAddr, m_db.m_dbNum, "", m_db.data());
					if (rc == 0) {
						m_db.m_dbSize = 2; // on all EPC class1 generation2 tags
						m_db.resize(m_db.m_dbNum * m_db.m_dbSize);
						emit dataRead();
					}
					m_mtxDB.unlock();

					if (rc != 0)
						setErrorText(rc);
				}
				break;

			case TagHandler::TH_Base::TYPE_ISO15693_NXP_ICODE_SLI:
				{
					TagHandler::TH_ISO15693_NXP_ICODE_SLI *tagIcodeSli = dynamic_cast<TagHandler::TH_ISO15693_NXP_ICODE_SLI *>(tagHandler);
					if (tagIcodeSli == NULL) {
						setErrorText("Fatal tag handler error");
						break;
					}

					m_mtxDB.lock();
					m_db.resize(MaxBufSize);
					rc = tagIcodeSli->ReadMultipleBlocks(m_db.m_dbAddr, m_db.m_dbNum, m_db.m_dbSize, m_db.data());
					if (rc == 0) {
						m_db.resize(m_db.m_dbNum * m_db.m_dbSize);
						emit dataRead();
					}
					m_mtxDB.unlock();

					if (rc != 0)
						setErrorText(rc);
				}
				break;

			case TagHandler::TH_Base::TYPE_ISO14443:
				{
					rc = m_reader.ITag.Select(tagHandler);
					if (rc != 0) {
						setErrorText(rc);
						break;
					}
					TagHandler::TH_Base *tagHandlerSel = m_reader.ITag.GetSelectedTagHandler();
					if (tagHandlerSel == NULL) {
						setErrorText("Fatal tag handler error");
						break;
					}
					TagHandler::TH_ISO14443_3_MIFARE_Classic *tagMifareClassic = dynamic_cast<TagHandler::TH_ISO14443_3_MIFARE_Classic *>(tagHandlerSel);
					if (tagMifareClassic == NULL) {
						setErrorText("Fatal tag handler error");
						break;
					}

					m_mtxDB.lock();
					rc = tagMifareClassic->Authent(m_db.m_dbAddr, m_db.m_keyType, m_db.m_key.toStdString());
					if (rc == 0) {
						m_db.resize(MaxBufSize);
						rc = tagMifareClassic->ReadMultipleBlocks(m_db.m_dbAddr, m_db.m_dbNum, m_db.m_dbSize, m_db.data());
					}
					if (rc == 0) {
						m_db.resize(m_db.m_dbNum * m_db.m_dbSize);
						emit dataRead();
					}
					m_mtxDB.unlock();

					if (rc != 0)
						setErrorText(rc);
				}
				break;
            case TagHandler::TH_Base::TYPE_ISO15693_NXP_ICODE_SLIX:
                {
                TagHandler::TH_ISO15693_NXP_ICODE_SLIX *tagIcodeSli = dynamic_cast<TagHandler::TH_ISO15693_NXP_ICODE_SLIX *>(tagHandler);
                if (tagIcodeSli == NULL) {
                    setErrorText("Fatal tag handler error");
                    break;
                }

                m_mtxDB.lock();
                m_db.resize(MaxBufSize);
                rc = tagIcodeSli->ReadMultipleBlocks(m_db.m_dbAddr, m_db.m_dbNum, m_db.m_dbSize, m_db.data());
                if (rc == 0) {
                    m_db.resize(m_db.m_dbNum * m_db.m_dbSize);
                    emit dataRead();
                }
                m_mtxDB.unlock();

                if (rc != 0)
                    setErrorText(rc);
                }
                break;
            case TagHandler::TH_Base::TYPE_ISO15693_NXP_ICODE_SLIX2:
            {
                TagHandler::TH_ISO15693_NXP_ICODE_SLIX2 *tagIcodeSlix2 = dynamic_cast<TagHandler::TH_ISO15693_NXP_ICODE_SLIX2 *>(tagHandler);
                if (tagIcodeSlix2 == NULL) {
                    setErrorText("Fatal tag handler error");
                    break;
                }

                m_mtxDB.lock();
                m_db.resize(MaxBufSize);
                rc = tagIcodeSlix2->ReadMultipleBlocks(m_db.m_dbAddr, m_db.m_dbNum, m_db.m_dbSize, m_db.data());
                if (rc == 0) {
                    m_db.resize(m_db.m_dbNum * m_db.m_dbSize);
                    emit dataRead();
                }
                m_mtxDB.unlock();

                if (rc != 0)
                    setErrorText(rc);
                }
                break;
			default:
                setErrorText("Unsupported tag type " + QString::number(m_db.m_handlerType));
				break;
		}
	}

	/* write a data block once */
	if (m_doWriteDB) {
		TagHandler::TH_Base *tagHandler = m_reader.ITag.GetTagHandler( m_db.m_uid.toStdString() );
		switch (m_db.m_handlerType) {
			case TagHandler::TH_Base::TYPE_EPC_CLASS1_GEN2:
				{
					TagHandler::TH_EPC_Class1_Gen2 *tagEpcC1G2 = dynamic_cast<TagHandler::TH_EPC_Class1_Gen2 *>(tagHandler);
					if (tagEpcC1G2 == NULL) {
						setErrorText("Fatal tag handler error");
						break;
					}

					m_mtxDB.lock();
					rc = tagEpcC1G2->WriteMultipleBlocks(m_db.m_bank, m_db.m_dbAddr, m_db.m_dbNum, "", m_db.data());
					if (rc == 0) {
						emit dataWritten();
					}
					m_mtxDB.unlock();

					if (rc != 0)
						setErrorText(rc);
				}
				break;

			case TagHandler::TH_Base::TYPE_ISO15693_NXP_ICODE_SLI:
				{
					TagHandler::TH_ISO15693_NXP_ICODE_SLI *tagIcodeSli = dynamic_cast<TagHandler::TH_ISO15693_NXP_ICODE_SLI *>(tagHandler);
					if (tagIcodeSli == NULL) {
						setErrorText("Fatal tag handler error");
						break;
					}

					m_mtxDB.lock();
					rc = tagIcodeSli->WriteMultipleBlocks(m_db.m_dbAddr, m_db.m_dbNum, m_db.m_dbSize, m_db.data());
					if (rc == 0) {
						emit dataWritten();
					}
					m_mtxDB.unlock();

					if (rc != 0)
						setErrorText(rc);
				}
				break;

			case TagHandler::TH_Base::TYPE_ISO14443:
				{
					rc = m_reader.ITag.Select(tagHandler);
					if (rc != 0) {
						setErrorText(rc);
						break;
					}
					TagHandler::TH_Base *tagHandlerSel = m_reader.ITag.GetSelectedTagHandler();
					if (tagHandlerSel == NULL) {
						setErrorText("Fatal tag handler error");
						break;
					}
					TagHandler::TH_ISO14443_3_MIFARE_Classic *tagMifareClassic = dynamic_cast<TagHandler::TH_ISO14443_3_MIFARE_Classic *>(tagHandlerSel);
					if (tagMifareClassic == NULL) {
						setErrorText("Fatal tag handler error");
						break;
					}

					m_mtxDB.lock();
					rc = tagMifareClassic->Authent(m_db.m_dbAddr, m_db.m_keyType, m_db.m_key.toStdString());
					if (rc == 0) {
						rc = tagMifareClassic->WriteMultipleBlocks(m_db.m_dbAddr, m_db.m_dbNum, m_db.m_dbSize, m_db.data());
					}
					if (rc == 0) {
						emit dataWritten();
					}
					m_mtxDB.unlock();

					if (rc != 0)
						setErrorText(rc);
				}
				break;
        case TagHandler::TH_Base::TYPE_ISO15693_NXP_ICODE_SLIX:
            {
                TagHandler::TH_ISO15693_NXP_ICODE_SLIX *tagIcodeSli = dynamic_cast<TagHandler::TH_ISO15693_NXP_ICODE_SLIX *>(tagHandler);
                if (tagIcodeSli == NULL) {
                    setErrorText("Fatal tag handler error");
                    break;
                }

                m_mtxDB.lock();
                rc = tagIcodeSli->WriteMultipleBlocks(m_db.m_dbAddr, m_db.m_dbNum, m_db.m_dbSize, m_db.data());
                if (rc == 0) {
                    emit dataWritten();
                }
                m_mtxDB.unlock();

                if (rc != 0)
                    setErrorText(rc);
            }
            break;
            case TagHandler::TH_Base::TYPE_ISO15693_NXP_ICODE_SLIX2:
            {
                TagHandler::TH_ISO15693_NXP_ICODE_SLIX2 *tagIcodeSlix2 = dynamic_cast<TagHandler::TH_ISO15693_NXP_ICODE_SLIX2 *>(tagHandler);
                if (tagIcodeSlix2 == NULL) {
                    setErrorText("Fatal tag handler error");
                    break;
                }

                m_mtxDB.lock();
                rc = tagIcodeSlix2->WriteMultipleBlocks(m_db.m_dbAddr, m_db.m_dbNum, m_db.m_dbSize, m_db.data());
                if (rc == 0) {
                    emit dataWritten();
                }
                m_mtxDB.unlock();

                if (rc != 0)
                    setErrorText(rc);
            }
            break;
			default:
				setErrorText("Unsupported tag type");
				break;
		}
	}

	closeConnection();
}



void ISOHostThread::OnNewReaderProtocol(const unsigned int uiDirection, const std::string sLogText)
{
	emit newProtocol(QString::fromStdString(sLogText));
}
