![](captures/obidisohost.PNG)

## A propos ##


|                   |                           |  
| ------------------| ------------------------- |  
| Title:            | OBID ISO HOST             |  
| Author:           | Colin Bouvry. FEIG.       |  
| Date:             | 2019-22-02                |    
| Version logiciel: | 1.0.2                     | 
| Version README:   | 1.0.2                     |  
|                   |                           |


OBID ISO HOST est un logiciel permettant de simplifier la gestion d'écriture et de lecture de tout type d'étiquette RFID. Il accélère le flux de travail, c'est un outil idéal pour gérer de nombreuses étiquettes.  
Il est compatible windows 7+, OSX et Linux.


## Feuille de route ##

### A faire ###
* Gestion page

### Version 1.0.2 (22/02/2019) ###

* Prise en charge des étiquettes SLIX2  
* Mise à jour doc gestion pagination

### Version 1.0.1 (17/01/2019) ###

* Prise en charge des fichiers de type .txt .hex et .csv
* Navigation facile dans le csv à l'aide des Spins ligne et colonne.

![](captures/CSV_SPIN.PNG)

### Version 1.0.0 (07/01/2019) ###

* Communication USB par defaut
* Ajout bouton "save" fichier hexadecimal
* Ajout bouton "load" fichier hexadecimal
* Prise en charge des étiquettes SLIX

### Version FEIG ###

* Version de base OBID ISO HOST depuis SDK FEIG avec QT (FEIG)

## Obtenir OBID ISO HOST ##

Vous pouvez télécharger la dernière version Windows ici :

<https://bitbucket.org/colinbouvry/isohostsample/downloads/>

                                                        
                            
## Tests ##

- Windows 10
- Transpondeur CPR30-USB
- Etiquette SLIX (SMARTRAC RaceTrack)
- CPRSTART

## CPRSTART ##

Logiciel livré par FEIG :   

![](captures/CPRSTART.PNG)

Protection en écriture du Lecteur :

![](captures/WRITE_PROTECTION.PNG)

Mettez WR-Options sur Tag option = 1 en sélectionnant EEPROM et/ou RAM puis cliquez sur le bouton write

 
## Types de transpondeur compatibles ##

- EPC Class 1 Generation 2
- NXP ICode SLi
- ISO 14443-3 MIFARE Classic (Identifiant et mot de passe)
- NXP ICode Slix
- NXP ICode Slix2


## Utilisation du logiciel ##

### Choix du type de communication ###

- Lancez le logiciel par double clique sur l'exécutable
- Sélectionnez le type de communication entre USB, TCP et COM
- Réglez les communications
- Cliquez sur le bouton Open communication puis sur le bouton Ok

![](captures/ISOHostSample_communication.PNG)

### Interface de lecture et écriture ###

![](captures/ISOHostSample.PNG)

- Posez le tag sur le lecteur.
- Réglez Bank en fonction du tag utilisé : 
	- DB-Addr : décalage au début en nombre de ligne
	- DB-Num : nombre de ligne
	- DB-Size : nombre de colonne
- Cliquez sur le bouton start, le tag doit être détecté et apparaître dans la liste.
- Sélectionnnez le tag dans la liste.
- Cliquez sur le bouton read pour lire les datas se trouvant dedans.
- Changez les données en hexadecimal directement dans le tableau à gauche ou en chargeant un fichier en cliquant sur le bouton load et en sélectionnant le fichier.
- Cliquez sur le bouton write pour écrire sur la mémoire du tag.
- Cliquez sur le bouton save pour écrire un fichier texte sur le disque dur.

### Gestion pagination ###

Attention au nombre de block par page configuré dans le firmware du lecteur.  
Si CFG Pages est configuré à 64, il ne sera pas possible de lire ou écrire plus de 64 lignes à la fois.Dans ce cas mettez DB-Addr à 64 pour écrire ou lire la deuxième page.

#### Depuis CPRSTART : ####

![](captures/reader_info.PNG)

#### ISOStart_ReaderMasterDatabase.xml : ####

![](captures/readermasterdatabase_origin.PNG)


## Etiquettes ##

### EXEMPLE RACETRACK LITE ##

Configuration maximale avec l'étiquette :

![](captures/STIX_SMARTRAC.PNG)

28 lignes * 4 colonnes soit 112 octets max soit 896 bits.

Cela correspond à la doc technique :  

RACETRACK LITE equipped with NXP ICODE SLIX provides 896 bits
voir doc 0061_SMARTRAC_RACETRACK_LITE.pdf

### Fichier ###

Exemple d'un fichier texte avec des caractères hexadécimals :

![](captures/EXEMPLE_HEXA.PNG)


## Compilation ##

### Installation ###

Copyright © FEIG ELECTRONIC GmbH, All Rights Reserved.
            Lange Strasse 4
            D-35781 Weilburg
            Federal Republic of Germany
            phone    : +49 6471 31090
            fax      : +49 6471 310999
            e-mail   : identification-support@feig.de
            Internet : http://www.feig.de

OBID and OBID i-scan are registered trademarks of FEIG ELECTRONIC GmbH

===============================
      ID ISC.SDK.Win

        V5.00.00
    
       2018-07-27

===============================

1. What is new

   The main Release Notes can be found in ReleaseNotes_Cpp.pdf

NOTE: To learn more about FEIG readers use our Windows Tools ISOStart V10.02.00 (or higher) or CPRStart V10.00.00 (or higher)

2. Installation

   Please follow the installation guide in Installation_Cpp_N70810-e-ID-B.pdf

3. First Steps

   To become familiar with the FEIG reader families, developers should read first the system manual of the reader.

   We recommend to read secondly the tutorial. Although it is not yet completed, it gives a structured introduction into the library stack 

   and the high-level APIs of the C++ class library.

   The migration guide (N80710-e-ID-B) contains necessary steps to migrate from old to new API.

Additionally, FEIG provides tools called ISOStart (for OBID i-scan) and CPRStart (for OBID classic-pro), which demonstrates all capabilities of each Reader Type.
These tools are a great help for Developers to understand the commands, configuration and Reader modes of an FEIG Reader.

4a. MFC-Samples using the fedm-core-api library. For Miscosoft Visual C++ 2012 (path: "sub-prj\sample-core-api_cpp")

- BRMSample_MultipleReaders (Multithreading Multi-Reader version of BRMSample)
- NotificationSample_MultipleReaders (Multithreading Multi-Reader version of FENotifySample)


4b. Command line C++ sample using the fedm-core-api library. For Miscosoft Visual C++ 2013 (path: "sub-prj\sample-core-api_cpp")

- FWUpdateSample (demonstrates the firmware update of a reader)

4c. Qt-Samples using the fedm-core-api library. For Miscosoft Visual C++ 2013 (path: "sub-prj\sample-core-api_qt-cpp")

- BRMSample (demonstrates the Buffered-Read-Mode for Readers supporting the Buffered-Read-Mode)
- ISOHostSample_with_TagHandler (demonstrates the use of ISO-Host-Commands by use of TagHandler classes)
- NotifySample (demonstrates the Notification-Mode)

5. Project Settings and Particularities

   Please check, if the work directory in project settings in property page DEBUG contains a content. If not, please type ..\..\..\..\sw-run\x86\vc110\Debug.
   
   
### QT Creator ###

![](captures/QT_creator.PNG)

## Licence ##

QT  
FEIG