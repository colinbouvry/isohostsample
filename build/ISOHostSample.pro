
TEMPLATE = app

win32 {
  PRJ_DIR = ..
  CONFIG += embed_manifest_exe
  DEFINES += _AFXEXT _CRT_SECURE_NO_DEPRECATE
  CONFIG(release, debug|release) {
    TARGET = ISOHostSampleVC120
    DESTDIR = $$_PRO_FILE_PWD_/release
    LIBS += $$PRJ_DIR/../../lib/fecom/build/windows/x86/vc110/Release/fecom.lib \
            $$PRJ_DIR/../../lib/feusb/build/windows/x86/vc110/Release/feusb.lib \
            $$PRJ_DIR/../../lib/fetcp/build/windows/x86/vc110/Release/fetcp.lib \
            $$PRJ_DIR/../../lib/feisc/build/windows/x86/vc110/Release/feisc.lib \
            $$PRJ_DIR/../../lib/fedm-core-api/build/windows/x86/vc120/Release/FedmIscCoreApiVC120.lib
  } 
  CONFIG(debug, debug|release) {
    TARGET = ISOHostSampleVC120d
    DESTDIR = $$_PRO_FILE_PWD_/debug
    LIBS += $$PRJ_DIR/../../lib/fecom/build/windows/x86/vc110/Debug/fecom.lib \
            $$PRJ_DIR/../../lib/feusb/build/windows/x86/vc110/Debug/feusb.lib \
            $$PRJ_DIR/../../lib/fetcp/build/windows/x86/vc110/Debug/fetcp.lib \
            $$PRJ_DIR/../../lib/feisc/build/windows/x86/vc110/Debug/feisc.lib \
            $$PRJ_DIR/../../lib/fedm-core-api/build/windows/x86/vc120/Debug/FedmIscCoreApiVC120d.lib
  }

} else:unix {
  PRJ_DIR = $$_PRO_FILE_PWD_/..
  LIB_DIR = /lib/x86_64-linux-gnu
  LIBS += $$LIB_DIR/libfecom.so \
          $$LIB_DIR/libfeusb.so \
          $$LIB_DIR/libfetcp.so \
          $$LIB_DIR/libfeisc.so \
          $$LIB_DIR/libFedmIscCore.so \
          $$LIB_DIR/libFedmIscService.so \
          $$LIB_DIR/libFedmIscCoreApi.so
  CONFIG(release, debug|release) {
    TARGET = ISOHostSample
    DESTDIR = ./release
    OBJECTS_DIR = $$DESTDIR
    MOC_DIR = $$DESTDIR
  }
  CONFIG(debug, debug|release) {
    TARGET = ISOHostSample
    DESTDIR = ./debug
    OBJECTS_DIR = $$DESTDIR
    MOC_DIR = $$DESTDIR
  }
}

QT += widgets
CONFIG += qt
DEFINES += _FEDM_NO_MFC_SUPPORT QT_DLL

INCLUDEPATH += $$PRJ_DIR/source \
               $$PRJ_DIR/../../lib/fecom/include \
			   $$PRJ_DIR/../../lib/feusb/include \
			   $$PRJ_DIR/../../lib/fetcp/include \
			   $$PRJ_DIR/../../lib/feisc/include \
			   $$PRJ_DIR/../../lib/fefu/include \
			   $$PRJ_DIR/../../lib/fetcl/include \
               $$PRJ_DIR/../../lib/fedm-core/include \
               $$PRJ_DIR/../../lib/fedm-core-api/include

HEADERS += $$PRJ_DIR/source/ISOHostSample.h \
           $$PRJ_DIR/source/ISOHostThread.h \
		   $$PRJ_DIR/source/feHexEdit.h \
		   $$PRJ_DIR/source/feAuthentMifareDialog.h \
           $$PRJ_DIR/source/feComPar.h

SOURCES += $$PRJ_DIR/source/main.cpp \
           $$PRJ_DIR/source/ISOHostSample.cpp \
		   $$PRJ_DIR/source/ISOHostThread.cpp \
		   $$PRJ_DIR/source/feHexEdit.cpp \
		   $$PRJ_DIR/source/feAuthentMifareDialog.cpp \
		   $$PRJ_DIR/source/feComPar.cpp
